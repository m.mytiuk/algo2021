#include <iostream>
#include <math.h>
using namespace std;

// Глобальні змінні
const int n = 40;
const int min = 1;
float *pole = new float[n];

// Прототипи
void generate_number(float min, float max);
void render_array(float pole[]);
void bubbleSort(float *array, int size);

int main(void)
{
    generate_number(1, 3000);
    bubbleSort(pole, n);
    render_array(pole);

    cin.get();
    return 0;
}

void generate_number(float min, float max)
{
    for (int i = 0; i < n; i++)
    {
        pole[i] = (min + 1) + (((float) rand()) / (float) RAND_MAX) * (max - (min + 1));
    }
}

void render_array(float pole[])
{
    for (int i = 0; i < n; i++)
        printf("%d :  %.3f\n", i, pole[i]);
}

void bubbleSort(float *array, int size)
{
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = 0; j < size - i - 1; j++)
        {
            if (array[j + 1] < array[j])
            {
                int tmp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = tmp;
            }
        }
    }
}