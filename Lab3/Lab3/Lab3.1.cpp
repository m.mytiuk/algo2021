#include <iostream>
using namespace std;

int factorial(int n);

int main()
{
    int n;

    cout << "Ведіть позитивне число від 0 до 20: ";
    cin >> n;
    cout << "Факторіал числа " << n << " = " << factorial(n);

    return 0;
}

int factorial(int n)
{
    if (n > 1)
        return n * factorial(n - 1);
    else
        return 1;
}