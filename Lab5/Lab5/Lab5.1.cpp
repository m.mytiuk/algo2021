#include <iostream>
#include <iomanip>
#include <locale.h>
using namespace std;

struct List
{
    int info;
    List *prev, *next;
};

// Функція для вставки елементу в список після елементу last
// Повертає адресу вставленого елементу
List *InsertElementInList(List *last, List *p)
{
    if (last && p)
    {
        p->prev = last;
        p->next = last->next;
        last->next = p;
        p->next->prev = p;
        return p;
    }
    else
        return NULL;
}

// Функція створює двухзв'язний список з двома додатковими head and tail
// Читаємо вхідну послідовність чисел з консолі, зупиняється читання після вводу 0
void CreateList(List *&head, List *&tail)
{
    head = new List;
    tail = new List;
    head->next = tail;
    tail->prev = head;
    int k;
    cout << "Введіть цілі числа. Для зупинки вводу введіть 0" << endl;
    cin >> k;
    List *last = head;
    while (k)
    {
        List *p = new List;
        p->info = k;
        last = InsertElementInList(last, p);
        cin >> k;
    }
    return;
}

// Функція друкує в консоль двухзв'язний список
void PrintList(List *head, List *tail)
{
    List *p = head->next;
    while (p != tail)
    {
        cout << setw(6) << p->info;
        p = p->next;
    }
    cout << endl;
    return;
}

void SelectionSortList(List *&head, List *&tail)
{
    List *head2 = new List;
    List *tail2 = new List;
    List *last = head2;

    head2->next = tail2;
    tail2->prev = head2;
    while (head->next != tail) // здійснюємо перебір допоки в початковому списку не закінчились елементи
    {
        // шукаємо мінімальний з елементів та видаляємо його
        List *min = head->next, *p = head->next;
        while (p != tail)
        {
            if (p->info < min->info)
                min = p;
            p = p->next;
        }
        // руйнуєо зв'язок, попередній і наступний елементи вказують один на одного не враховуючи min
        min->next->prev = min->prev;
        min->prev->next = min->next;

        last = InsertElementInList(last, min); //знайдений елемент додаємо в початок нового списку
    }
    head = head2;
    tail = tail2;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    List *head, *tail = NULL;
    CreateList(head, tail);
    cout << "Початковий список" << endl;
    PrintList(head, tail);
    SelectionSortList(head, tail);
    cout << "Відсортований список" << endl;
    PrintList(head, tail);

    system("pause");
}