#include <iostream>
#include <iomanip>
#include <locale.h>
using namespace std;

void insertionSort(int arr[], int n)
{
    int i, key, j;
    for (i = 1; i < n; i++)
    {
        key = arr[i];
        j = i - 1;

        // Переміщуємо елементи масиву, більші за ключ
        // на один індекс вперед
        while (j >= 0 && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

// Додаткова ф-ція для друку масиву
void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; i++)
        cout << arr[i] << " ";
    cout << endl;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    int arr[] = {12, 11, 13, 5, 6};
    int n = sizeof(arr) / sizeof(arr[0]);
    cout << "Початковий масив" << endl;
    printArray(arr, n);
    insertionSort(arr, n);
    cout << "Відсортований масив" << endl;
    printArray(arr, n);

    system("pause");
}