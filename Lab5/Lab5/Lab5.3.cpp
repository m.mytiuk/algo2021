#include <iostream>
#include <iomanip>
#include <locale.h>
using namespace std;

struct List
{
    int info;
    List *prev, *next;
};

// Функція для вставки елементу в список після елементу last
// Повертає адресу вставленого елементу
List *InsertElementInList(List *last, List *p)
{
    if (last && p)
    {
        p->prev = last;
        p->next = last->next;
        last->next = p;
        p->next->prev = p;
        return p;
    }
    else
        return NULL;
}

// Функція створює двухзв'язний список з двома додатковими head and tail
// Читаємо вхідну послідовність чисел з консолі, зупиняється читання після вводу 0
void CreateList(List *&head, List *&tail)
{
    head = new List;
    tail = new List;
    head->next = tail;
    tail->prev = head;
    int k;
    cout << "Введіть цілі числа. Для зупинки вводу введіть 0" << endl;
    cin >> k;
    List *last = head;
    while (k)
    {
        List *p = new List;
        p->info = k;
        last = InsertElementInList(last, p);
        cin >> k;
    }
    return;
}

// Функція друкує в консоль двухзв'язний список
void PrintList(List *head, List *tail)
{
    List *p = head->next;
    while (p != tail)
    {
        cout << setw(6) << p->info;
        p = p->next;
    }
    cout << endl;
    return;
}

void InsertionSortList(List *head)
{

    List *curr = nullptr, *prev = nullptr;
    for (curr = head->next; curr->next; curr = curr->next)
    {
        int tmp = curr->info;
        for (prev = curr->prev; prev && prev->info > tmp; prev = prev->prev)
        {
            prev->next->info = prev->info;
        }
        prev->next->info = tmp;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");

    List *head, *tail = NULL;
    CreateList(head, tail);
    cout << "Початковий список" << endl;
    PrintList(head, tail);
    ///////////////
    InsertionSortList(head);
    cout << "Відсортований список" << endl;
    PrintList(head, tail);

    system("pause");
}