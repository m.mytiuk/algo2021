#include <stdio.h>
#include "math.h"
#include "stdlib.h"
#include <time.h>

short freqCheck(short array[], int num);

int main()
{
    unsigned int a = 2147483629, c = 2147483587, m = pow(2, 31), len = 40000, i;
    unsigned int *values;
    values = (unsigned int *)malloc(sizeof(unsigned int) * len);
    values[0] = time(NULL);
    for (i = 0; i < len - 1; i++)
        values[i + 1] = (a * values[i] + c) % m;
    short *newValues;
    newValues = (short *)malloc(sizeof(short) * len);
    for (i = 0; i < len; i++)
        newValues[i] = (float)values[i] / (m - 1) * 100;
    short freq[100];
    printf("\nЧастота інтервалів появи випадкових величин:\n");
    for (i = 0; i < 100; i++)
    {
        freq[i] = freqCheck(newValues, i);
        printf("Value #%d = %d\n", i + 1, freq[i]);
    }
    double staticPrbbl[100];
    printf("\nСтатистчна ймовірність появи випадкових величин:\n");
    for (i = 0; i < 100; i++)
    {
        staticPrbbl[i] = ((double)freq[i] / len);
        printf("Value #%d = %.4f\n", i, staticPrbbl[i]);
    }
    double mathExp = 0;
    for (i = 0; i < 100; i++)
        mathExp += i * staticPrbbl[i];
    printf("\nМатематичне очікування випадкових величин: %.6f\n", mathExp);
    double dyspersion = 0;
    for (i = 0; i < 100; i++)
        dyspersion += pow((i - mathExp), 2) * staticPrbbl[i];
    printf("\nДисперcія випадкових величин: %f\n", dyspersion);
    printf("\nСередньоквадратичне відхилення впадкових величин: %f\n", sqrt(dyspersion));
    return 0;
}

short freqCheck(short array[], int num)
{
    short freq = 0;
    for (int i = 0; i < 4000; i++)
        if (array[i] == num)
            freq++;
    return freq;
}