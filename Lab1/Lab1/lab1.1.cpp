#include <stdio.h>

struct Time
{
	unsigned short day : 6;
	unsigned short month : 6;
	unsigned short year : 1;
	unsigned short hour : 11;
	unsigned short minute : 11;
	unsigned short seconds : 11;
};

int main()
{
	unsigned short arr = 0;
	Time n;
	printf("Enter day: "); scanf("%hu", &arr); n.day = arr;
	printf("Enter month: "); scanf("%hu", &arr); n.month = arr;
	printf("Enter year: "); scanf("%hu", &arr); n.year = arr;
	printf("Enter hour: "); scanf("%hu", &arr); n.hour = arr;
	printf("Enter minutes: "); scanf("%hu", &arr); n.minute = arr;
	printf("Enter seconds: "); scanf("%hu", &arr); n.seconds = arr;
	int memory = sizeof(Time);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("! - - - Memory in bytes: %d - - - !\n", memory);
	printf("Date: %hu.%hu.%hu\nTime: %hu:%hu:%hu\n", n.day, n.month, n.year, n.hour, n.minute, n.seconds);
	return 0;
}
