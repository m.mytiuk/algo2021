#include <stdio.h>
#include "stdlib.h"

typedef int elemtype;

struct elem
{
    elemtype value;    // Значення зміной
    struct elem *next; // Ссилка на след елемент
    struct elem *prev; // Ссылка на пред.елемент
};

struct myList
{
    struct elem *head; // Перший елементів списка
    struct elem *tail; // Останій елементів списка
    size_t size;       // Кол-во елементів в списку
};

typedef struct elem cNode;
typedef struct myList cList;

cList *createList(void);
void deleteList(cList *list);
bool isEmptyList(cList *list);
int pushFront(cList *list, elemtype *data);
int popFront(cList *list, elemtype *data);
int pushBack(cList *list, elemtype *data);
int popBack(cList *list, elemtype *data);
cNode *getNode(cList *list, int index);
void printList(cList *list, void (*fun)(elemtype *));
void printNode(elemtype *value);
int addElementInPosition(myList *list, elemtype *data, int index);
int deleteElementFromPosition(myList *list, int index);

int main()
{
    char choice;
    cList *mylist;
    mylist = createList();
    do
    {
        elemtype tmp;
        printf("1. Add element to list;\n2. Delete element from list;\n3. Show list;\n4. Delete list;\n0. Quit\n > ");
        scanf("%hu", &choice);
        switch (choice)
        {
        case 1:
        {
            char ch;
            printf("\n1. - Push front;\n2. - Push back;\n3. - Add element in any position;\n > ");
            scanf("%hu", &ch);
            if (ch == 1)
            {
                printf("Your value: ");
                scanf("%d", &tmp);
                pushFront(mylist, &tmp);
            }
            else if (ch == 2)
            {
                printf("Your value: ");
                scanf("%d", &tmp);
                pushBack(mylist, &tmp);
            }
            else if (ch == 3)
            {
                printf("Your value: ");
                scanf("%d", &tmp);
                int position;
                printf("Position of the element: ");
                scanf("%d", &position);
                addElementInPosition(mylist, &tmp, position);
            }
            else
                printf("Error404\n");
            break;
        }
        case 2:
        {
            char ch;
            elemtype val;
            printf("1. - Delete first;\n2. - Delete last element;\n3. - Delete element from any position\n > ");
            scanf("%hu", &ch);
            if (ch == 1)
                popFront(mylist, &tmp);
            else if (ch == 2)
                popBack(mylist, &tmp);
            else if (ch == 3)
            {
                int index;
                printf("Choose element which you want to delete: ");
                scanf("%d", &index);
                deleteElementFromPosition(mylist, index);
            }
            else
                printf("Error404");
            break;
        }
        case 3:
        {
            printf("Your list: \n");
            printList(mylist, printNode);
            break;
        }
        case 4:
        {
            deleteList(mylist);
            printf("Your list is clear!\n");
            break;
        }
        default:
            printf("Error404. Enter another value!\n");
            break;
        }
    } while (choice != 0);
    return 0;
}

int addElementInPosition(myList *list, elemtype *data, int index)
{
    elem *node = (elem *)malloc(sizeof(elem));
    node->value = *data;
    if (index == 0)
    {
        if (isEmptyList(list))
        {
            node->next = NULL;
            node->prev = NULL;
            list->head = node;
            list->tail = node;
        }
        else
        {
            node->prev = NULL;
            list->head->prev = node;
            node->next = list->head;
            list->head = node;
        }
        list->size++;
    }
    else
    {
        if (isEmptyList(list))
            return -2;
        if (index != list->size)
        {
            if (index >= list->size || index <= 0)
                return -2;
            elem *prevNode = (elem *)malloc(sizeof(elem));
            if (index < list->size / 2)
            {
                prevNode = list->head;
                for (int i = 0; i < index - 1; i++)
                {
                    prevNode = prevNode->next;
                }
                node->next = prevNode->next;
                node->prev = prevNode->prev;
                prevNode->next = node;
            }
            else
            {
                prevNode = list->tail;
                for (int i = list->size; i > index; i--)
                {
                    prevNode = prevNode->prev;
                }
                node->prev = prevNode;
                node->next = prevNode->next;
                prevNode->next = node;
            }
            list->size++;
            return 0;
        }
        else
        {
            elem *prevNode = (elem *)malloc(sizeof(elem));
            prevNode = list->tail;
            prevNode->next = node;
            node->prev = prevNode;
            list->tail = node;
            list->tail->next = NULL;
            list->size++;
        }
    }
}

int deleteElementFromPosition(myList *list, int index)
{
    elem *temp = (elem *)malloc(sizeof(elem));
    elem *nodeToRemove = (elem *)malloc(sizeof(elem));
    if (isEmptyList(list))
        return -1;
    if (index == 0)
    {
        nodeToRemove = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        nodeToRemove->next = NULL;
        nodeToRemove->value = NULL;
        free(nodeToRemove);
        list->size--;
        return 0;
    }
    else
    {
        {
            if (index != list->size - 1)
                if (index < list->size / 2)
                {
                    temp = list->head;
                    for (int i = 1; i < index - 1; i++)
                    {
                        temp = temp->next;
                    }
                    nodeToRemove = temp->next;
                    temp->next = nodeToRemove->next;
                    nodeToRemove->next->prev = temp;
                    nodeToRemove->prev = NULL;
                    nodeToRemove->next = NULL;
                    nodeToRemove->value = NULL;
                    free(nodeToRemove);
                }
                else
                {
                    temp = list->tail;
                    for (int i = list->size; i > index + 1; i--)
                    {
                        temp = temp->prev;
                    }
                    nodeToRemove = temp->prev;
                    temp->prev = nodeToRemove->prev;
                    nodeToRemove->prev->next = temp;
                    nodeToRemove->prev = NULL;
                    nodeToRemove->next = NULL;
                    nodeToRemove->value = NULL;
                    free(nodeToRemove);
                }
            list->size--;
            return 0;
            temp = list->head;
            for (int i = 1; i < index - 1; i++)
            {
                temp = temp->next;
            }
            nodeToRemove = temp->next;
            temp->next = nodeToRemove->next;
            nodeToRemove->next->prev = temp;
            nodeToRemove->prev = NULL;
            nodeToRemove->next = NULL;
            nodeToRemove->value = NULL;
            free(nodeToRemove);
            temp = list->tail;
            for (int i = list->size; i > index + 1; i--)
            {
                temp = temp->prev;
            }
            nodeToRemove = temp->prev;
            temp->prev = nodeToRemove->prev;
            nodeToRemove->prev->next = temp;
            nodeToRemove->prev = NULL;
            nodeToRemove->next = NULL;
            nodeToRemove->value = NULL;
            free(nodeToRemove);
            nodeToRemove = list->tail;
            list->tail = nodeToRemove->prev;
            list->tail->next = NULL;
            nodeToRemove->prev = NULL;
            nodeToRemove->next = NULL;
            nodeToRemove->value = NULL;
            free(nodeToRemove);
            list->size--;
        }
    }
}

// Створення списка
cList *createList(void)
{
    cList *list = (cList *)malloc(sizeof(cList));
    if (list)
    {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// Видалення списка
void deleteList(cList *list)
{
    cNode *head = list->head;
    cNode *next = NULL;
    while (head)
    {
        next = head->next;
        free(head);
        head = next;
    }
    free(list);
    list = NULL;
}

// Перевірка на пустоту
bool isEmptyList(cList *list)
{
    return ((list->head == NULL) || (list->tail == NULL));
}

// Додавання вузла в початкок списка
int pushFront(cList *list, elemtype *data)
{
    cNode *node = (cNode *)malloc(sizeof(cNode));
    if (!node)
    {
        return (-1);
    }
    node->value = *data;
    node->next = list->head;
    node->prev = NULL;

    if (!isEmptyList(list))
    {
        list->head->prev = node;
    }
    else
    {
        list->tail = node;
    }
    list->head = node;

    list->size++;
    return (0);
}

// Видалення вузла з початку списка
int popFront(cList *list, elemtype *data)
{
    cNode *node;

    if (isEmptyList(list))
    {
        return (-2);
    }

    node = list->head;
    list->head = list->head->next;

    if (!isEmptyList(list))
    {
        list->head->prev = NULL;
    }
    else
    {
        list->tail = NULL;
    }

    *data = node->value;
    list->size--;
    free(node);

    return (0);
}

// Додаваня нового вузла в кінець списку
int pushBack(cList *list, elemtype *data)
{
    cNode *node = (cNode *)malloc(sizeof(cNode));
    if (!node)
    {
        return (-3);
    }

    node->value = *data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list))
    {
        list->tail->next = node;
    }
    else
    {
        list->head = node;
    }
    list->tail = node;

    list->size++;
    return (0);
}

// Видалення вузла з кінця списку
int popBack(cList *list, elemtype *data)
{
    cNode *node = NULL;

    if (isEmptyList(list))
    {
        return (-4);
    }

    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list))
    {
        list->tail->next = NULL;
    }
    else
    {
        list->head = NULL;
    }

    *data = node->value;
    list->size--;
    free(node);

    return (0);
}

// Читання вузла списку
cNode *getNode(cList *list, int index)
{
    cNode *node = NULL;
    int i;

    if (index >= list->size)
    {
        return (NULL);
    }

    if (index < list->size / 2)
    {
        i = 0;
        node = list->head;
        while (node && i < index)
        {
            node = node->next;
            i++;
        }
    }
    else
    {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index)
        {
            node = node->prev;
            i--;
        }
    }

    return node;
}

// Вивід списка в консоль
void printList(cList *list, void (*func)(elemtype *))
{
    cNode *node = list->head;

    if (isEmptyList(list))
    {
        return;
    }

    while (node)
    {
        func(&node->value);
        node = node->next;
    }
}

void printNode(elemtype *value)
{
    printf("%d\n", *((int *)value));
}
